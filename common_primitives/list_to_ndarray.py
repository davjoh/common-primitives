import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('ListToNDArrayPrimitive',)

Inputs = container.List
Outputs = container.ndarray


class Hyperparams(hyperparams.Hyperparams):
    pass


class ListToNDArrayPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which converts a list into a numpy array.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '40ff1396-0725-4cf9-b7b9-c6eca6237f65',
            'version': '0.1.0',
            'name': "List to ndarray converter",
            'python_path': 'd3m.primitives.data.ListToNDArray',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        dataframe = container.ndarray(inputs)

        # TODO: Remove once fixed in core package and released.
        #       See: https://gitlab.com/datadrivendiscovery/d3m/issues/144
        dataframe.metadata = self._update_metadata(dataframe.metadata, self)

        return base.CallResult(dataframe)

    @classmethod
    def _update_metadata(cls, inputs_metadata: metadata_base.DataMetadata, source: typing.Any) -> metadata_base.DataMetadata:
        outputs_metadata = inputs_metadata

        selector: metadata_base.ListSelector = [metadata_base.ALL_ELEMENTS]

        while 'structural_type' in outputs_metadata.query(selector):
            metadata = outputs_metadata.query(selector)
            if issubclass(metadata['structural_type'], (container.List, container.ndarray)):
                outputs_metadata = outputs_metadata.update(selector, {
                    'structural_type': metadata_base.NO_VALUE,
                }, source=source)
            else:
                break

            selector.append(metadata_base.ALL_ELEMENTS)

        return utils.set_table_metadata(outputs_metadata, source=source)

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        outputs_metadata = inputs_metadata.update((), {
            'structural_type': container.ndarray,
        }, source=cls)

        outputs_metadata = cls._update_metadata(outputs_metadata, cls)

        return outputs_metadata
