from typing import Dict, Tuple
from d3m.container.list import List
from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer
import os
import numpy as np  # type: ignore
from sklearn.feature_extraction.text import CountVectorizer  # type: ignore

Inputs = List
Outputs = ndarray


class Hyperparams(hyperparams.Hyperparams):
    ngram_range = hyperparams.Hyperparameter[Tuple[int, int]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=(1, 1),
        description="The lower and upper boundary of the range of n-values for different n-grams to be extracted. "
    )

    stop_words = hyperparams.Hyperparameter[bool](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=True,
        description='Whether to remove stopwords or not.'
    )


class BagOfWords(TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    Primitive which takes a list of filenames to text documents and returns
    a matrix N * V of word counts where N is number of documents and V
    is the vocabulary size.
    """
    __author__ = 'Oxford DARPA D3M Team, Rob Zinkov <zinkov@robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
         'id': 'caca109d-31ec-415d-997b-d5902e99f5a2',
         'version': '0.1.0',
         'name': 'Image reader',
         'keywords': ['text', 'txt', 'featurization', 'ngram'],
         'source': {
            'name': 'common-primitives',
            'contact': 'mailto:zinkov@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'
                           .format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.BagOfWords',
         'algorithm_types': ['NUMERICAL_METHOD'],
         'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)
        self._vectorizer = CountVectorizer(input='filename',
                                           ngram_range=self.hyperparams['ngram_range'],
                                           stop_words='english' if self.hyperparams['stop_words'] else None)

    def produce(self, *,
                inputs: List,
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        if len(inputs) < 1:
            raise ValueError('Expecting a list of at least one string with file names (received an empty list).')
        X = self._vectorizer.fit_transform(inputs)
        return CallResult(X)
