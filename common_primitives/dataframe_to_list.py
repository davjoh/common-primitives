import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('DataFrameToListPrimitive',)

Inputs = container.DataFrame
Outputs = container.List


class Hyperparams(hyperparams.Hyperparams):
    pass


class DataFrameToListPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which converts a pandas dataframe into a list of rows.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '901ff55d-0a0a-4bfd-8195-3a947ba2a8f5',
            'version': '0.1.0',
            'name': "DataFrame to list converter",
            'python_path': 'd3m.primitives.data.DataFrameToList',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        return base.CallResult(container.List(inputs))

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        # DataFrame is converted to a list of lists.
        outputs_metadata = inputs_metadata.update((), {
            'structural_type': container.List,
        }, source=cls)
        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS,), {
            'structural_type': container.List,
        }, source=cls)

        return outputs_metadata
