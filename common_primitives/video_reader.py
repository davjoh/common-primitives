
from typing import Dict, Tuple
from d3m import container
from d3m.container.list import List
from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer
import os
import numpy as np  # type: ignore
import cv2  # type: ignore


Inputs = List
Outputs = ndarray


class Hyperparams(hyperparams.Hyperparams):
    resize_to = hyperparams.Hyperparameter[Tuple[int,int]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=(0,0),
        description='Resize all loaded videos to this size. Expecting a tuple '+
                    '(H, W) where H is height and W is width. The number of '+
                    'channels (e.g., grayscale, RGB) is not affected. If left '+
                    'as the default value (0, 0), no resizing will be applied.'
    )


class VideoReader(TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    Primitive which takes a list of file names to video files and returns a
    list of length N of Numpy arrays of shape:
        N x F x H x W x C
    where N is the number of videos, F is the number of frames, C is the number of channels in an video
    (e.g., C = 1 for grayscale, C = 3 for RGB), H is the height, and W is the
    width. All videos are expected to be of the same shape and channel
    characteristics.
    """
    __author__ = 'University of Michigan, Eric Hofesmann, Madan Ravi Ganesh, Nathan Louis'
    metadata = metadata_module.PrimitiveMetadata({
         'id': 'a29b0080-aeff-407d-9edb-0aa3eefbde01',
         'version': '0.1.0',
         'name': 'Video reader',
         'keywords': ['video', 'avi', 'mp4'],
         'source': {
            'name': 'common-primitives',
            'contact': 'mailto:davjoh@umich.edu'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'
                           .format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.VideoReader',
         'algorithm_types': ['NUMERICAL_METHOD'],
         'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)
        self._resize_to = self.hyperparams['resize_to']


    def _load_video(self, path):
        """
        Loads a video from a given path

        Arguments:
            :path: Path to the video to load

        Returns:
            :vid: Video loaded from the specified location
        """

        cap = cv2.VideoCapture(path)
        frames = []

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break

            else:
                frames.append(frame)

        cap.release()

        return np.array(frames)




    def produce(self, *,
                inputs: List,
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        # TODO consider caching the outputs 
        if len(inputs) < 1:
            raise ValueError('Expecting a list of at least one string with file names (received an empty list).')
        videos = container.List()
        video_shape = None
        for file_name in inputs:
            vid = self._load_video(file_name)
            out_vid = []
            for frame in vid:
                if self._resize_to[0] != 0 or self._resize_to[1] != 0:
                    out_vid.append(cv2.resize(frame, (self._resize_to[1], self._resize_to[0])))
                else:
                    out_vid.append(frame)
                    if video_shape is None:
                        video_shape = vid.shape[1:] #Ignore the number of frames
                    elif video_shape != vid.shape[1:]:
                        raise ValueError('Expecting all videos to be of the same shape {} (received {}).'.format(video_shape, vid.shape))
            videos.append(np.array(out_vid))

        return CallResult(videos)
