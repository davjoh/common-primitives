import unittest

import numpy as np

from common_primitives.k_means import KMeans, Hyperparams


class KMeansTestCase(unittest.TestCase):
    def test_fit_produce(self):
        """
        KMeans fit and produce results.
        """

        hy = Hyperparams.defaults()
        hy1 = dict(hy)
        hy1['n_clusters'] = 2
        hy2 = Hyperparams(hy1)

        a = KMeans(hyperparams=hy2)
        train_x = np.array([[1, 2], [1, 4], [1, 0],
                            [4, 2], [4, 4], [4, 0]])
        a.set_training_data(inputs=train_x)
        a.fit()
        # You can use a list of multiple samples.
        result = a.produce(inputs=[np.array([0, 0]), np.array([4, 4])]).value
        self.assertTrue(np.array_equal(result, np.array([0, 1])))

        # Or you can use one array which has multiple samples along the first dimension.
        result = a.produce(inputs=np.array([[0, 0], [4, 4]])).value
        self.assertTrue(np.array_equal(result, np.array([0, 1])))


if __name__ == '__main__':
    unittest.main()
